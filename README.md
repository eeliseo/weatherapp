# README #

### Quick summary ###

This a very simple Weather App, which gives the forecast for today and 5 days ahead.
When the app launches for the first time, it will prompt the user to write a city name to stablish a location point. Subsequently, the user can add a city and look at the weather forecast for that location at any given time.

### Design ###

A very minimalistic design. It uses the default images that the API provides. Also, it shows not all the info that the API offers, but only the essential.
It supports iOS 9 and later. The reason of this is because it uses StackViews for layout. It also supports both orientations, landscape and portrait, and it's universal compatible for iPhone and iPads.

### Cool things that I would have done given the time ###

* The API gives a lot of information to play with. I would look at it and provide more detailed info, like sunrise, sunset, speed of wind, thermal sensation, etc.
* I would have like to play with the option to change between Celsius and Farenheit units.
* Multi-language support (at least spanish and english)
- Cache of images: If the API gives me an image for the first time (i.e. weather icon representation) I could save it locally, and use it the next time without having to download it again.
* Request control: The worldweatheronline API documentation recommends to do a a new request each hour or so. I would have like to manage a way to store the last response, and last request time in order to spare unnecesary data usage.
* Better data storage method for more possibilities like the one said above. Right now it uses NSUSerDefaults to persist the data, which limits the things that I could persist.
* Custom icons: Better styles icons and images to give the app more personality.