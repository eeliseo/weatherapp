//
//  main.m
//  WeatherApp
//
//  Created by Eduardo on 29/11/16.
//  Copyright © 2016 Eduardo Eliseo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
