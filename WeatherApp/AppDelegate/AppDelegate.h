//
//  AppDelegate.h
//  WeatherApp
//
//  Created by Eduardo on 29/11/16.
//  Copyright © 2016 Eduardo Eliseo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

